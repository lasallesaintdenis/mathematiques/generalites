#!/usr/bin/env python
from turtle import *
from time import sleep

def branche(longueur, ordre):
	if ordre == 0: forward(longueur)
	else:
		for angle in [60,-120,60]:
			branche(longueur/3, ordre - 1)
			left(angle)
		branche(longueur/3, ordre - 1)

def flocon(longueur, ordre):
	for i in range(3):
		branche(longueur, ordre)
		right(120)

flocon(100, 3)
sleep(1)
