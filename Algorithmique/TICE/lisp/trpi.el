(defun trpi (n)
  (interactive "nn-ième TRPI")
  (let (a 1) (m 1) (c 1)
    (while (< m n)
      (while (<= (* c c) (+ (+ (* (* a a) 2) (* 2 a)) 1))
        (if (= (* c c) (+ (+ (* (* a a) 2) (* 2 a)) 1))
            (setq m (1+ m)))
        (setq c (1+ c)))
      (setq a (1+ a)))
    (message "%d" (1- a))
    (message "%d" a )
    (message "%d" (1- c) )))
trpi

