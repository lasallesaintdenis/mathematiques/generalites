/* $Id: main.c 183 2008-01-24 00:22:22Z endymion $ */

/* À compiler avec "gcc -Wall -pedantic -lm main.c -o main" */

#include <stdio.h>
#include <stdlib.h>
/*#include <math.h>*/


int main(void)
{
  int n=1,N;
  int a=1,c=1;
  scanf("%d",&N);
    while(n<N)
      {
/* 	finding c | sqrt(c)==floor(c) <=> \exists l | l*l = c */
	while( c*c <= (2*a*a+2*a+1) ) 
	{
	  if( c*c == (2*a*a+2*a+1) )
	  {
	    n++;
	  }
	  c++;
	}
	a++;
      }
  printf("%d %d %d\n",a-1,a,c-1);
  return 0;
}

/* Tester avec dc -e "$1 2 ^ $2 2 ^ + $3 2 ^ - p" ou $1 $2 $3 sont les résultats de la commande précédente */
