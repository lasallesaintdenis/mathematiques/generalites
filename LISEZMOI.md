# Dépôt de cours, plans et autres développements

## Précision sur le contenu

Ce dépôt contient des cours, des devoirs, et autres documents relatifs à
mon travail en tant qu'enseignant. Il est hébergé par mes propres soins
sur https://cgit.thetys-retz.net/ en source et je ne désespère pas
trouver un moyen efficace de le publier de façon structurée.

Ce dépôt peut être récupéré en intégralité (maths) ou seulement pour les
niveaux qui vous intéressent : (maths/<niveau>)

## Architecture du dépôt

Ce dépôt utilise les «subtrees» de git. Ainsi, ils ont été ajoutés avec

  * git remote add <nom> <url>
  * git fetch <nom>
  * git subtree add -P <nom>/ <nom> master

L'envoi des modification se fait soit :

  * globalement avec ``git push``
  * individuellement avec ``git subtree push -P <nom>/ <nom> master``

On peut également récupérer les informations depuis overleaf.com :
  * ``git subtree pull -P <chemin/total/nom>/ <nom> master``

Inspiré de [](https://gist.github.com/dwilliamson/e9b1ba3c684162c5a931)

En cas d'échec du push : ``git push TS-SVT `git subtree split -P TS-SVT master`:master --force``
Voir [](https://stackoverflow.com/questions/13756055/git-subtree-subtree-up-to-date-but-cant-push#15623469)
