À faire :
  Appliquer le code suivant :

  ```
    for i in liste de fichier ; do
      perl -0777pe 's/.*?\\begin\{document\}[^\n]*\n?//s' fichier.tex > nfichier.tex
      mv nfichier.tex fichier.tex
    done
  ```
  et ajouter le nouvel entête :

  ```
    \documentclass[a4paper,12pt,frenchb]{article}

    \input{../../commons.tex.inc}

    \title{}
    \author{}
    \date{}

    \begin{document}
  ```

On peut aussi faire un entête spécifique pour les présentations beamer
