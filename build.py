#!/usr/bin/env python3
import subprocess
import sys,os
from shutil import copyfile
import re
from filecmp import cmp
opts = "--pdf --unsafe"


filename = sys.argv[1]
complete_filename = os.path.abspath(filename)
filename_stripped = os.path.split(filename)[-1]
complete_dirname = os.path.dirname(complete_filename)
dirname = os.path.split(complete_dirname)[1]

command = ['rubber'] + opts.split(' ') + [complete_filename]

os.chdir(complete_dirname)
subprocess.check_output(command)


dest = '/home/users/endymion/Documents/JBS/Docs/'
cdest = os.path.join(dest,dirname,filename_stripped)

print("Copie du tex :",complete_filename,cdest)
copyfile(complete_filename,cdest)

cfilenamepdf = re.sub('\.tex$','.pdf',complete_filename)
cdestpdf = re.sub('\.tex$','.pdf',cdest)

print("Copie du pdf :", cfilenamepdf, cdestpdf)
copyfile(cfilenamepdf, cdestpdf)


dependances = subprocess.check_output(['rubber-info', '--deps',
    filename_stripped])
for fichier in map(lambda x: x.decode().strip('\n'), dependances.split(b' ')):
    if fichier[:6] != 'Source':
        if not cmp(fichier,re.sub('Mathématiques','JBS/Docs',fichier)):
            print("Copie d'une dépendance tex :",fichier)
            copyfile(fichier,re.sub('Mathématiques','JBS/Docs',fichier))


command = ['rubber', '--clean'] + [complete_filename]
subprocess.check_output(command)
