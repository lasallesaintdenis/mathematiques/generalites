% vim: set ft=tex :
\usepackage[utf8x]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{fontawesome}
%\usepackage{kpfonts}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage[colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

%\usepackage[a4paper,vmargin=12.7mm,hmargin=6.35mm,includefoot,includehead]{geometry}
\usepackage[top=6.35mm,bottom=6.35mm,left=2cm,right=2cm,includehead,includefoot]{geometry}

%\PassOptionsToPackage{framemethod=tikz}{mdframed}
\usepackage{tipfr}
\usepackage[tikz]{bclogo}
\usepackage{tikz}
\usetikzlibrary{circuits.ee.IEC}

\usepackage{tkz-euclide}
\usepackage{tkz-tab}
\usetkzobj{all}


\usepackage{amsmath,amsfonts,amssymb}
\usepackage{mathrsfs}

\usepackage{amsthm}
\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\usepackage{tabularx}

\usepackage{titling}
\usepackage{titlesec}

\usepackage{multicol}

\usepackage{xsim}
\xsimsetup{
  %counter-format = {qu[1] :} ,
  %headings = block-subtitle ,
  %points/name = {pt/s} ,
  solution/pre-hook = \mdframed ,
  solution/post-hook = \endmdframed ,
  blank/style = dotted ,
  %points/separate-bonus = true,
  }

% \DeclareInstance{exsheets-heading}{block-subtitle-new}{default}{
% indent-first = false,
% join = {
% title[r,B]number[l,B](.333em,0pt) ;
% title[r,B]subtitle[l,B](1em,0pt)
% } ,
% attach = {
% main[l,vc]title[l,vc](0pt,0pt) ;
% main[r,vc]points[l,vc](\marginparsep,0pt)
% } ,
% subtitle-post-code = {
%   ~\textbf{(\GetQuestionProperty{points}{\CurrentQuestionID} points)}} ,
% number-post-code = {\IfQuestionSubtitleF{
%   ~\textbf{(\GetQuestionProperty{points}{\CurrentQuestionID} points)}}},
%}

%\usepackage{exsheets-listings}

  \usepackage{listings}

\usepackage{eurosym}

\usepackage{sectsty}

\usepackage{lastpage}

\usepackage{marginnote}

\usepackage{wrapfig}

\usepackage{esvect}
\usepackage[extdef]{delimset}

\usepackage{enumitem}
\setlist{noitemsep}
%\setlist[1]{\labelindent=\parindent} % < Usually a good idea
\setlist[itemize]{leftmargin=*}
\setlist[itemize,1]{label=$\triangleright$}
\setlist[enumerate]{labelsep=*, leftmargin=1.5pc}
\setlist[enumerate,1]{label=\textbf{\arabic*.}, ref=\arabic*}
\setlist[enumerate,2]{label=\emph{\alph*}),
ref=\theenumi.\emph{\alph*}}
\setlist[enumerate,3]{label=\roman*), ref=\theenumii.\roman*}
\setlist[description]{font=\sffamily\bfseries}


\usepackage{fancyhdr}
\pagestyle{fancy}

\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi

\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
\lfoot{\textsl{\footnotesize{Lycée \textsc{LaSalle Saint-Denis}}}}
\rfoot{\footnotesize{Page \thepage/ \pageref{LastPage}}}
\rhead{}
\lhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}


\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\usepackage{array,multirow,makecell}
\setcellgapes{1pt}
\makegapedcells
\newcolumntype{R}[1]{>{\raggedleft\arraybackslash }b{#1}}
\newcolumntype{L}[1]{>{\raggedright\arraybackslash }b{#1}}
\newcolumntype{C}[1]{>{\centering\arraybackslash }b{#1}}


\usepackage{multicol}
\setlength{\columnseprule}{0pt}

% Définition des ensembles
\newcommand{\R}{\mathbf{R}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\Z}{\mathbf{Z}}
% Définition des notations
\newcommand{\p}{\mathcal{P}}
\newcommand{\diff}{\!\mathrm{d}}

\newcommand{\repOij}{$(O,\vv{\imath},\vv{\jmath})$}

% ne devraient plus être utilisés
\newcommand{\Vecteur}{\overrightarrow}
\newcommand{\norme}[1]{\left\lVert #1 \right\rVert}
\newcommand{\vabs}[1]{\left\lvert #1 \right\rvert}
\newcommand{\ioo}[2]{\left]#1~;~#2\right[}

\usepackage{draftwatermark}
\SetWatermarkText{Brouillon}

\newcommand{\ligne}[1]{%
  \begin{tikzpicture}[baseline=1cm]
    \draw[white] (0,#1+0.8) -- (\linewidth,#1+0.8) ;
    \foreach \i in {1,...,#1}
    { \draw[dotted] (0,\i) -- (\linewidth,\i) ; }
    \draw[white] (0,0.6) -- (\linewidth,0.6) ;
  \end{tikzpicture}%
}

\usepackage{babel}
