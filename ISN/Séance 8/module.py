#! /usr/bin/python2.7

# -*- encoding: utf-8 -*-

fichier=open('fichier.txt','r')
chaine=" ".join(fichier.readlines()[:]).decode('utf-8')
fichier.close()

fichier=open('un_sur_deux.txt','w')
fichier.write(" ".join(chaine.split()[0::2]))
fichier.close()
