\documentclass[12pt,a4paper,frenchb]{article}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{ntheorem}
\usepackage{amsmath}
\usepackage{amsfonts}

\usepackage{kpfonts}

\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}

\pdfminorversion 7
\pdfobjcompresslevel 3

\usepackage{tabularx}

\usepackage{pgf}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usetkzobj{all}
\usepackage[top=1.0cm,bottom=1.5cm,left=2cm,right=2cm,includefoot]{geometry}

\usepackage{lastpage}

\usepackage{wrapfig}

\usepackage{listings}

\usepackage[autolanguage]{numprint}
\newcommand{\np}{\numprint}

\usepackage{babel}

\makeatletter

\newcommand{\notp}[1]{\global\def\@notp{#1}}

\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi

\newcommand{\anneescol}{\number\count1 - \number\count2}

\renewcommand{\@evenhead}%
{\renewcommand{\arraystretch}{1.5}
\begin{tabularx}{\linewidth}{|p{0.22\linewidth}|X|p{0.22\linewidth}|}\hline
    \@notp{} \@date & \hfill Informatique et Sciences du Numérique \hfill~& \hfill JBS \anneescol \\ \hline
\end{tabularx}\\
}
\renewcommand{\@oddhead}{\@evenhead}

\renewcommand{\@evenfoot}%
        {\hfil \upshape \small page {\thepage} de \pageref{LastPage}}
\renewcommand{\@oddfoot}{\@evenfoot}

\renewcommand{\maketitle}%
{\begin{center}
  \framebox{%
    \begin{minipage}{0.8\linewidth}%
      \begin{center}%
        \Large \@title %
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}

\usepackage{framed}

\newenvironment{objectif}{%
  \begin{framed}
    \setlength{\parindent}{0pt}
  {\bfseries Objectif :}\vspace{\z@}}
  {\end{framed}}

\makeatother

\headsep=35.0pt

%\theoremstyle{break}
\theorembodyfont{\normalfont}
\renewtheorem{exercice}{Exercice}

\theoremstyle{nonumberplain}
\newtheorem{probleme}{Problème}

% Mise en forme des labels dans les énumérations
\renewcommand{\labelenumi}{\textbf{\theenumi.}}
\renewcommand{\labelenumii}{\textbf{\theenumii)}}
\renewcommand{\theenumi}{\arabic{enumi}}
\renewcommand{\theenumii}{\alph{enumii}}

\renewcommand{\labelitemi}{$\bullet$}

\setlength{\parsep}{0pt}
\setlength{\parskip}{5pt}
%\setlength{\parindent}{0pt}
\setlength{\itemsep}{7pt}

\usepackage{multicol}
\setlength{\columnseprule}{0pt}

\everymath{\displaystyle\everymath{}}

\notp{Séance 3}
\title{Coder de l'information}
\author{\bsc{Jumel}}
\date{10/2013}

\begin{document}
\maketitle
\begin{objectif}
  \begin{itemize}
    \item connaître différentes représentation d'un entier ;
    \item passer de l'une à l'autre des représentations d'un même entier
      ;
    \item connaître les expressions «bit», «octet» et «mot» ;
    \item coder un caractère de l'alphabet ;
    \item coder une couleur / une image.
  \end{itemize}
\end{objectif}

En mathématiques, il existe plusieurs façons de représenter un même
nombre : on appelle ça la base d'un nombre. En informatique, on utilise
plus particulièrement la base 2 et la base 8. Pourquoi ?

\section{Un exemple pour comprendre}

Un peu de retour en arrière à l'époque où enfants découvrions les
nombres. On écrivait de fortes égalités pleines de sens comme :
\[ 2345 = 2\times 1000 + 3\times 100 + 4\times 10 + 5\times 1 \]
Un peu plus tard, au collège, on apprenait que $1000=10^3$ et l'égalité
précédente pouvait se réécrire :
\[ 2345 = 2\times 10^3 + 3\times 10^2 + 4\times 10^1 + 5\times 10^0 \]
On parle de pour notre expression habituelle des nombres de la base 10.
Compléter l'expression d'un nombre dans une base $b$, $b\in\mathbf{N}$ :
\[ a_0\times\dots^0 + a_1\times\dots^{\dots} + \cdots +
a_n\times\dots^{\dots} \]
avec la condition suivante :
\[\forall k \in \llbracket0;n\rrbracket, a_k < \dots \]
Sous ces conditions, indiquer quels sont les caractères utilisés en base
2, en base 8, en base 16.
\vspace{2cm}
\begin{exercice}
Changements de bases

  \begin{enumerate}
    \item
      \begin{enumerate}
        \item Écrire les nombres suivants en base 2 : \np{1024},
          \np{128}, \np{239}.
        \item Écrire les nombres suivants en base 10 : 1000 ; 11111 ;
          1010101.
      \end{enumerate}
    \item
      \begin{enumerate}
        \item Écrire les nombres suivants en base 16 : \np{64},
          \np{128}, \np{73}.
        \item Écrire les nombres suivants en base 10 : 10, 1f, c4.
      \end{enumerate}
  \end{enumerate}
\end{exercice}

\section{«bit», «octet», «mot»}
Un bit est la plus petite information codable, qui correspond à la
présence ou l'absence de signal. Mathématiquement, elle est codée par 0
ou 1. Attention à ne pas confondre avec un «byte», qui est un $n$-uplet.

Parmi les $n$-uplets particuliers, on distingue le 8-uplet ou
\emph{octet}. Il s'agit du regroupement de 8 bits qui permet de coder
256 valeurs différentes, c'est à dire d'exprimer un entier à deux
chiffres en base 16.

Un «mot» est la taille maximale (en bits ou en octets) qu'un processeur
peut traiter par cycle. Plus le mot est long, plus les instructions
seront «grandes» et plus le processeur sera «performant».
Parmi les processeurs «grand public», on a successivement vu des
processeurs traitant des mots de 16 bits, puis de 32 bits et enfin de 64
bits qui se généralisent depuis le début des années 2000.

\section{Coder un caractère}
\subsection{La table ASCII}
ASCII signifie American Standard Code for Information Interchange. Il
s'agit d'une table associative de caractères permettant de représenter
les 128 caractères les plus usuels (alphanumériques, et caractères de
contrôle). C'est un codage sur 7 bits, bien que la plus part des
ordinateurs travaillent avec des octets. Le 8\ieme{} bit est
généralement mis à 0.

Le code trivial suivant peremt de constater la longueur d'un caractère
sous un système GNU/Linux :
\lstset{basicstyle=\small,language=C}
\lstinputlisting{sizeofchar.c}

\subsection{D'autres façon de coder les caractères}
\begin{exercice}Découvrir d'autres codages de caractères

  \begin{enumerate}
    \item Afficher les documents
      \url{http://jumel.net/ISN/Séance\%203/1.html} et
      \url{http://jumel.net/ISN/Séance\%203/2.html}. Ànalyser l'entête
      du document et pointer la différence fondamentale. Les
      caractères sont-ils bien rendus ?
    \item Afficher les documents
      \url{http://jumel.net/ISN/Séance\%203/3.html} et
      \url{http://jumel.net/ISN/Séance\%203/4.html}. Que constate-t-on
      dans ces documents ? Quelle est la différence avec les
      documents précédents ?
    \item Afficher et analyser le document
      \url{http://jumel.net/ISN/Séance\%203/5.html}. Que manque-t-il
        dans ce document ? Comment sont représentés les caractères
        «spéciaux» ?
      Comment sont-ils codés ?
  \end{enumerate}
\end{exercice}

\section{Coder une couleur}

On admet ici qu'une couleur est le mélange des trois composantes rouge,
verte et bleue. L'intensité de chacune des composantes est représenté
par un entier entre 0 et 255.
La façon la plus simple de coder un tel entier est de le représenter par
un octet. Il faut donc 3 octets pour représenter une couleur. Cela
représente donc \hspace{2cm} combinaisons possibles. On parle aussi de
couleurs codées sur 24 bits.

Ainsi la représentation la plus simple d'une image de type «bitmap» est
un tableau d'octets comportant pour chacune des positions la
combinaisons des intensités lumineuses. Une telle image est ainsi non
compressée.

\end{document}

